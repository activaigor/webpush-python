import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="webpush",
    version="0.2.8",
    author="Igor Rizhyi",
    author_email="igorrizhyi@gmail.com",
    description="A common python libraries and apps for WebPush project",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/activaigor/webpush-python",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
        'PyJWT==1.7.1',
        'Django>=2.1.15',
        'djangorestframework>=3.10.3',
        'schematics==2.1.0'
    ]
)
