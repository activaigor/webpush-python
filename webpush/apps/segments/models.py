from django.db import models
from schematics.exceptions import DataError

from webpush.apps.segments.schema import ChannelSegment
from webpush.lib.json.fields import BaseTempDictField
from webpush.lib.json.models import ModelWithJsonFields


class SegmentField(BaseTempDictField):

    def get_model(self) -> ChannelSegment:
        try:
            segment = ChannelSegment(self._data)
            segment.validate()
        except DataError:
            return ChannelSegment({})
        else:
            return segment


class BaseSegmentModel(ModelWithJsonFields):
    payload_json = models.TextField(null=True, default=None, blank=True)
    payload = SegmentField(persistent_field_name='payload')

    class Meta:
        abstract = True

    @property
    def instance(self) -> ChannelSegment:
        return self.payload.get_model()
