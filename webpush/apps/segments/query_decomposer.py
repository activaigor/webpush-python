from typing import List
from django.db.models import Q

from webpush.apps.segments.schema import ChannelSegmentElement
from webpush.lib.statements.decomposer.base import BaseBlockDecomposer, BaseDecomposerFunc
from webpush.lib.statements.decomposer.results import DjangoQueryDecomposeResult


class SegmentDecomposerFunc(BaseDecomposerFunc):

    @classmethod
    def and_operator(cls, elements: List[ChannelSegmentElement]) -> DjangoQueryDecomposeResult:
        query = None
        for el in elements:
            if query is None:
                query = cls._convert_element_into_query(el)
            else:
                query = query & cls._convert_element_into_query(el)
        return DjangoQueryDecomposeResult(value=query)

    @classmethod
    def or_operator(cls, elements: List[ChannelSegmentElement]) -> DjangoQueryDecomposeResult:
        query = None
        for el in elements:
            if query is None:
                query = cls._convert_element_into_query(el)
            else:
                query = query | cls._convert_element_into_query(el)
        return DjangoQueryDecomposeResult(value=query)

    @classmethod
    def default_operator(cls, elements: list) -> DjangoQueryDecomposeResult:
        return DjangoQueryDecomposeResult(value=cls._convert_element_into_query(elements[0]))

    @staticmethod
    def _convert_element_into_query(element: ChannelSegmentElement) -> Q:
        raise NotImplementedError()


class SegmentBlockDecomposer(BaseBlockDecomposer):

    blocks: List['SegmentBlockDecomposer'] = None

    class Meta:
        decomposer_func = SegmentDecomposerFunc

    def decompose(self) -> Q:
        return super(SegmentBlockDecomposer, self).decompose()
