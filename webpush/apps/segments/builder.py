from typing import List

from webpush.apps.segments.query_decomposer import SegmentBlockDecomposer
from webpush.apps.segments.schema import ChannelSegmentBlock, ChannelSegmentElement
from webpush.lib.statements.decomposer.builder.base import BaseDecomposerBlockBuilder


class SegmentDecomposerBlockBuilder(BaseDecomposerBlockBuilder):

    block_source: ChannelSegmentBlock = None

    class Meta:
        decomposer = SegmentBlockDecomposer

    def _get_sub_block_sources(self) -> List[ChannelSegmentBlock]:
        return [b for b in self.block_source.inner_blocks]

    def _get_elements(self) -> List[ChannelSegmentElement]:
        return [e for e in self.block_source.elements]

    def _get_union_operator(self) -> str:
        return self.block_source.union_operator

    def _get_elements_union_operator(self) -> str:
        return self.block_source.elements_union_operator

    def _get_blocks_union_operator(self) -> str:
        return self.block_source.blocks_union_operator
