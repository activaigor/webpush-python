from typing import List

from webpush.apps.segments.schema import ChannelSegment


class CanCreateSegments:

    @staticmethod
    def create_simple_segment_with_channels(channels: List[int]) -> ChannelSegment:
        return ChannelSegment({'channel_ids': channels})
