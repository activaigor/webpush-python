import abc
import logging
from datetime import datetime
from typing import Any, Tuple


class IStringFormat(metaclass=abc.ABCMeta):

    @classmethod
    @abc.abstractmethod
    def to_primitive(cls, val_str: str) -> Any:
        pass


class BaseStringFormat(IStringFormat):

    mask: str = None

    @classmethod
    def to_primitive(cls, val_str: str) -> Any:
        try:
            return cls._to_primitive(val_str)
        except Exception as exc:
            logging.warning(f'There is a problem while trying to convert value_str with {cls}: {exc}')
            return

    @classmethod
    def _to_primitive(cls, val_str: str) -> Any:
        raise NotImplementedError()


class PlainStringFormat(BaseStringFormat):

    @classmethod
    def _to_primitive(cls, val_str: str) -> str:
        return val_str


class KeyValuePair(BaseStringFormat):

    @classmethod
    def _to_primitive(cls, val_str: str) -> Tuple[str, str]:
        key, val = val_str.split('=')
        return str(key), str(val)


class DateStringFormat(BaseStringFormat):

    mask = '%Y-%m-%d'

    @classmethod
    def _to_primitive(cls, val_str: str) -> datetime.date:
        return datetime.strptime(val_str, cls.mask)


class DateTimeStringFormat(BaseStringFormat):
    mask = '%Y-%m-%dT%H:%M:%S'

    @classmethod
    def _to_primitive(cls, val_str: str) -> datetime:
        return datetime.strptime(val_str, cls.mask)


class IntStringFormat(BaseStringFormat):

    @classmethod
    def _to_primitive(cls, val_str: str) -> Any:
        return int(val_str)


class StringFormats:

    DATE = DateStringFormat
    DATE_TIME = DateTimeStringFormat
    INT = IntStringFormat
