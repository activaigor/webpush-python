import abc

import jwt
from django.contrib.auth import get_user_model
from django.core.exceptions import MultipleObjectsReturned

from rest_framework import exceptions
from rest_framework.authentication import BaseAuthentication, get_authorization_header
from rest_framework.exceptions import AuthenticationFailed

from webpush.lib.builder import UserJWTBuilder
from webpush.lib.instances import UserJWTInstance

User = get_user_model()


class BaseJWTUserAuthentication(BaseAuthentication, metaclass=abc.ABCMeta):
    keyword = 'Bearer'

    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None

        if len(auth) == 1:
            msg = 'Invalid token header. No credentials provided.'
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = 'Invalid token header. Token string should not contain spaces.'
            raise exceptions.AuthenticationFailed(msg)

        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = 'Invalid token header. Token string should not contain invalid characters.'
            raise exceptions.AuthenticationFailed(msg)

        jwt_token = self._parse_token(token)

        try:
            user = self._get_user_from_token(jwt_token)
        except (User.DoesNotExist, MultipleObjectsReturned):
            raise AuthenticationFailed('User with such email does not exists')

        return user, jwt_token.token

    @classmethod
    @abc.abstractmethod
    def _get_user_from_token(cls, token: UserJWTInstance) -> User:
        pass

    @staticmethod
    def _parse_token(token) -> UserJWTInstance:
        try:
            return UserJWTBuilder.decode(token)
        except jwt.ExpiredSignature:
            raise AuthenticationFailed('Signature has expired')
        except jwt.InvalidTokenError:
            raise AuthenticationFailed('Invalid token')


class JWTUserAuthentication(BaseJWTUserAuthentication):

    @classmethod
    def _get_user_from_token(cls, token: UserJWTInstance) -> User:
        return User.objects.get(email=token.email)


class JWTForceUserAuthentication(BaseJWTUserAuthentication):

    @classmethod
    def _get_user_from_token(cls, token: UserJWTInstance) -> User:
        user, _ = User.objects.get_or_create(email=token.email)
        return user


class JWTUserWithPermsAuthentication(JWTForceUserAuthentication):

    @classmethod
    def _get_user_from_token(cls, token: UserJWTInstance):
        user = super(JWTUserWithPermsAuthentication, cls)._get_user_from_token(token)
        if hasattr(user, 'set_jwt_permissions'):
            user.set_jwt_permissions(token.permissions)
        return user
