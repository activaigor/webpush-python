from unittest import TestCase

from webpush.lib.statements.decomposer.builder.array import ArrayDecomposerBlockBuilder


class DecomposerBuilderFromArrayTest(TestCase):

    def setUp(self) -> None:
        self.source = {
            'blocks': [{
                'elements': ['a > 1', 'a < 3'],
                'elements_union': 'and'
            }, {
                'elements': ['c > 5', 'c < 8'],
                'elements_union': 'and'
            }],
            'block_union': 'or',
            'elements': ['b == 1', 'b == 3'],
            'elements_union': 'or',
            'union': 'and'
        }

    def test_builder_works_properly(self):
        builder = ArrayDecomposerBlockBuilder(block_source=self.source)
        res = builder.build()
        decomposed_value = res.decompose()
        self.assertEqual('((a > 1 and a < 3) or (c > 5 and c < 8)) and (b == 1 or b == 3)', decomposed_value)
