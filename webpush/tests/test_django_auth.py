from datetime import timedelta
from unittest import mock

from django.test import TestCase
from mixer.backend.django import mixer
from rest_framework.exceptions import AuthenticationFailed

from webpush.apps.users.auth.user import JWTUserWithPermsAuthentication
from webpush.apps.users.models import CustomUser
from webpush.lib.builder import UserJWTBuilder
from webpush.lib.conf import JWTConfig


class TestDjangoUserAuth(TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        JWTConfig.set_secret('secret')
        JWTConfig.set_expiration(timedelta(days=2))
        super(TestDjangoUserAuth, cls).setUpClass()

    def setUp(self) -> None:
        self.email = 'user@example.com'
        self.user = mixer.blend(CustomUser, email=self.email)
        from webpush.apps.users.auth.user import JWTUserAuthentication
        self.handler = JWTUserAuthentication()
        builder = UserJWTBuilder(consumer_id=self.user.id, email=self.email)
        builder.build()
        capitalize_builder = UserJWTBuilder(consumer_id=self.user.id, email=self.email.upper())
        capitalize_builder.build()
        foreign_jwt = UserJWTBuilder(consumer_id=self.user.id, email='foreigner@example.com')
        foreign_jwt.build()
        self.valid_jwt = builder.token
        self.valid_foreign_jwt = foreign_jwt.token
        self.valid_capitalize_jwt = capitalize_builder.token
        self.invalid_jwt = 'invalid_token'
        self.valid_request = mock.MagicMock(META={
            'HTTP_AUTHORIZATION': f'{self.handler.keyword} {self.valid_jwt}'
        })
        self.foreign_request = mock.MagicMock(META={
            'HTTP_AUTHORIZATION': f'{self.handler.keyword} {self.valid_foreign_jwt}'
        })
        self.valid_request_capitalize_email = mock.MagicMock(META={
            'HTTP_AUTHORIZATION': f'{self.handler.keyword} {self.valid_capitalize_jwt}'
        })
        self.invalid_request = mock.MagicMock(META={
            'HTTP_AUTHORIZATION': f'{self.handler.keyword} {self.invalid_jwt}'
        })
        self.request_without_auth = mock.MagicMock(META={
            'UNKNOWN': 'SOME_VALUE'
        })
        self.invalid_headers_request = mock.MagicMock(META={
            'HTTP_AUTHORIZATION': 'SOMETHING_WRONG_HERE'
        })
        self.invalid_headers_request_2 = mock.MagicMock(META={
            'HTTP_AUTHORIZATION': f'{self.handler.keyword} token with spaces'
        })

    def test_success_auth(self):
        user, _ = self.handler.authenticate(self.valid_request)
        self.assertIsNotNone(user)
        self.assertEqual(user, self.user)

    def test_success_auth_for_token_generated_with_capitalize_email(self):
        user, _ = self.handler.authenticate(self.valid_request_capitalize_email)
        self.assertIsNotNone(user)
        self.assertEqual(user, self.user)

    def test_auth_user_not_found(self):
        with self.assertRaises(AuthenticationFailed):
            self.handler.authenticate(self.foreign_request)

    def test_auth_with_invalid_token(self):
        with self.assertRaises(AuthenticationFailed):
            self.handler.authenticate(self.invalid_request)

    def test_request_without_auth(self):
        result = self.handler.authenticate(self.request_without_auth)
        self.assertIsNone(result)

    def test_auth_with_invalid_header(self):
        result = self.handler.authenticate(self.invalid_headers_request)
        self.assertIsNone(result)

    def test_auth_with_invalid_header_2(self):
        with self.assertRaises(AuthenticationFailed):
            self.handler.authenticate(self.invalid_headers_request_2)


class TestDjangoForceUserAuth(TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        JWTConfig.set_secret('secret')
        JWTConfig.set_expiration(timedelta(days=2))
        super(TestDjangoForceUserAuth, cls).setUpClass()

    def setUp(self) -> None:
        self.email = 'user@example.com'
        self.user = mixer.blend(CustomUser, email=self.email)
        from webpush.apps.users.auth.user import JWTForceUserAuthentication
        self.handler = JWTForceUserAuthentication()
        builder = UserJWTBuilder(consumer_id=self.user.id, email=self.email)
        builder.build()
        capitalize_builder = UserJWTBuilder(consumer_id=self.user.id, email=self.email.upper())
        capitalize_builder.build()
        foreign_jwt = UserJWTBuilder(consumer_id=self.user.id, email='foreigner@example.com')
        foreign_jwt.build()
        self.valid_jwt = builder.token
        self.valid_foreign_jwt = foreign_jwt.token
        self.valid_capitalize_jwt = capitalize_builder.token
        self.invalid_jwt = 'invalid_token'
        self.valid_request = mock.MagicMock(META={
            'HTTP_AUTHORIZATION': f'{self.handler.keyword} {self.valid_jwt}'
        })
        self.foreign_request = mock.MagicMock(META={
            'HTTP_AUTHORIZATION': f'{self.handler.keyword} {self.valid_foreign_jwt}'
        })
        self.valid_request_capitalize_email = mock.MagicMock(META={
            'HTTP_AUTHORIZATION': f'{self.handler.keyword} {self.valid_capitalize_jwt}'
        })
        self.invalid_request = mock.MagicMock(META={
            'HTTP_AUTHORIZATION': f'{self.handler.keyword} {self.invalid_jwt}'
        })
        self.request_without_auth = mock.MagicMock(META={
            'UNKNOWN': 'SOME_VALUE'
        })
        self.invalid_headers_request = mock.MagicMock(META={
            'HTTP_AUTHORIZATION': 'SOMETHING_WRONG_HERE'
        })
        self.invalid_headers_request_2 = mock.MagicMock(META={
            'HTTP_AUTHORIZATION': f'{self.handler.keyword} token with spaces'
        })

    def test_success_auth(self):
        user, _ = self.handler.authenticate(self.valid_request)
        self.assertIsNotNone(user)
        self.assertEqual(user, self.user)

    def test_success_auth_for_token_generated_with_capitalize_email(self):
        user, _ = self.handler.authenticate(self.valid_request_capitalize_email)
        self.assertIsNotNone(user)
        self.assertEqual(user, self.user)

    def test_auth_user_for_foreign_user(self):
        user, _ = self.handler.authenticate(self.foreign_request)
        self.assertIsNotNone(user)
        self.assertNotEqual(user, self.user)

    def test_auth_with_invalid_token(self):
        with self.assertRaises(AuthenticationFailed):
            self.handler.authenticate(self.invalid_request)

    def test_request_without_auth(self):
        result = self.handler.authenticate(self.request_without_auth)
        self.assertIsNone(result)

    def test_auth_with_invalid_header(self):
        result = self.handler.authenticate(self.invalid_headers_request)
        self.assertIsNone(result)

    def test_auth_with_invalid_header_2(self):
        with self.assertRaises(AuthenticationFailed):
            self.handler.authenticate(self.invalid_headers_request_2)


class TestJWTWithPermsAuthentication(TestCase):

    def setUp(self) -> None:
        self.email = 'user@example.com'
        self.user = mixer.blend(CustomUser, email=self.email)
        self.handler = JWTUserWithPermsAuthentication()
        self.valid_jwt = UserJWTBuilder(consumer_id=999, email=self.email)
        self.valid_jwt.build()
        self.valid_new_jwt = UserJWTBuilder(consumer_id=999, email='new@example.com')
        self.valid_new_jwt.build()
        self.valid_request = mock.MagicMock(META={
            'HTTP_AUTHORIZATION': f'{self.handler.keyword} {self.valid_jwt.token}'
        })
        self.valid_request_new_user = mock.MagicMock(META={
            'HTTP_AUTHORIZATION': f'{self.handler.keyword} {self.valid_new_jwt.token}'
        })

    def test_auth_with_valid_jwt_and_existed_user(self):
        user, _ = self.handler.authenticate(self.valid_request)
        self.assertIsNotNone(user)
        self.assertEqual(user, self.user)
        self.assertIsNotNone(user._jwt_permissions)

    def test_auth_with_valid_jwt_and_new_user(self):
        user, _ = self.handler.authenticate(self.valid_request_new_user)
        self.assertIsNotNone(user)
        self.assertNotEqual(user, self.user)
        self.assertIsNotNone(user._jwt_permissions)
