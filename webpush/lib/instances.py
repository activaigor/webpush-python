from datetime import datetime
from typing import List, Any


class JWTTypes:
    USER = 'user'
    SERVICE = 'service'


class JWTRefInstance(dict):
    USER_TYPES = (JWTTypes.USER, JWTTypes.SERVICE)

    def __init__(self, **kwargs):
        self.is_authenticated = True
        self.consumer_id = kwargs.get('consumer_id')
        self.exp = kwargs.get('exp')
        self.consumer_type = kwargs.get('consumer_type')
        if self.consumer_type:
            assert self.consumer_type in self.USER_TYPES, f'Invalid consumer_type = {self.consumer_type}'
        self.token = kwargs.get('token')
        self.email = kwargs.get('email')
        if self.email is not None:
            self.email = self.email.lower()
        super(JWTRefInstance, self).__init__(**kwargs)

    def set_expire(self, expire_date: datetime) -> None:
        self.exp = expire_date

    def set_email(self, email: str) -> None:
        self.email = email

    def to_representation(self) -> dict:
        return self.__dict__


class JWTInstance(JWTRefInstance):

    def __init__(self, **kwargs):
        self.data = kwargs.get('data', {})
        self.permissions = JWTPermissions(**kwargs.get('permissions', {}))
        super(JWTInstance, self).__init__(**kwargs)

    def _set_data(self, key: str, value: Any) -> None:
        self.data[key] = value


class ServiceJWTInstance(JWTInstance):

    def __init__(self, **kwargs):
        self.consumer_type = JWTTypes.SERVICE
        super(ServiceJWTInstance, self).__init__(**kwargs)


class UserJWTInstance(JWTInstance):

    def __init__(self, **kwargs):
        self.consumer_type = JWTTypes.USER
        super(UserJWTInstance, self).__init__(**kwargs)

    def set_channels(self, channels: List[int]) -> None:
        self._set_data('channels', channels)

    def set_permissions(self, channels: List['JWTChannelPermissions'] = None):
        self.permissions['channels'] = channels


class JWTPermissions(dict):

    def __init__(self, **kwargs):
        self.channels = [JWTChannelPermissions(**c) for c in kwargs.get('channels') or []]
        super(JWTPermissions, self).__init__(**kwargs)


class JWTChannelPermissions(dict):

    def __init__(self, **kwargs):
        self.channel_id = kwargs.get('channel_id')
        self.can_edit = kwargs.get('can_edit')
        self.can_send = kwargs.get('can_send')
        self.can_retrieve = kwargs.get('can_retrieve')
        super(JWTChannelPermissions, self).__init__(**kwargs)
