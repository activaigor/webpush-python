import abc
import json
import logging

from django.db.models import Model, TextField


class BaseTempJsonField(abc.ABC):

    @abc.abstractmethod
    def __init__(self, persistent_field_name: str):
        self._persistent_field_name = persistent_field_name
        self._data = None
        self._instance = None
        self.init_value = None

    @property
    def empty(self) -> bool:
        return not self._data

    def attach_to_instance(self, instance: Model) -> None:
        persistent_field = {f.name: f for f in instance._meta.fields}.get(self._persistent_field_name)
        assert persistent_field is not None, f"{self} is not related to {instance}"
        assert isinstance(persistent_field, TextField), f"{persistent_field} is not a TextField"
        self._instance = instance
        self.init_value = self.get_persistent_field()

    def get_persistent_field(self) -> str:
        assert self._instance is not None, "You need to attach field to instance first"
        return getattr(self._instance, self._persistent_field_name)

    def update_persistent_field(self) -> None:
        assert self._instance is not None, "You need to attach field to instance first"
        value = json.dumps(self._data)
        setattr(self._instance, self._persistent_field_name, value)
        self.init_value = value

    def update(self):
        assert self._instance is not None, "You need to attach field to instance first"
        self.attach_to_instance(self._instance)

    def to_representation(self):
        assert self._instance is not None, "You need to attach field to instance first"
        try:
            result = json.loads(getattr(self._instance, self._persistent_field_name))
        except (json.JSONDecodeError, TypeError, ValueError):
            return None
        else:
            return result


class BaseTempListField(BaseTempJsonField):

    _data: list = None

    def __init__(self, persistent_field_name: str):
        super(BaseTempListField, self).__init__(persistent_field_name)

    def attach_to_instance(self, instance: Model) -> None:
        super(BaseTempListField, self).attach_to_instance(instance)
        try:
            self._data = json.loads(self.get_persistent_field())
        except (json.JSONDecodeError, TypeError, ValueError):
            self._data = []
        if not isinstance(self._data, list):
            logging.warning(f"{instance}.{self._persistent_field_name} is not a list. It's value will be overwritten")
            self._data = []

    def to_representation(self) -> list:
        return super(BaseTempListField, self).to_representation() or []


class BaseTempDictField(BaseTempJsonField):

    _data: dict = None

    def __init__(self, persistent_field_name: str):
        super(BaseTempDictField, self).__init__(persistent_field_name)

    def attach_to_instance(self, instance: Model) -> None:
        super(BaseTempDictField, self).attach_to_instance(instance)
        try:
            self._data = json.loads(self.get_persistent_field())
        except (json.JSONDecodeError, TypeError, ValueError):
            self._data = {}
        if not isinstance(self._data, dict):
            logging.warning(f"{instance}.{self._persistent_field_name} is not a dict. It's value will be overwritten")
            self._data = {}

    def to_representation(self) -> dict:
        return super(BaseTempDictField, self).to_representation() or {}
