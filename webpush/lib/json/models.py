import copy
from typing import Dict, List

from django.db import models
from django.db.models.base import ModelBase

from webpush.lib.json.fields import BaseTempJsonField


class ModelWithJsonBase(ModelBase):

    def __new__(mcs, name, bases, attrs, **kwargs):
        attrs['_meta_json_fields'] = dict([(a[0], a[1]) for a in attrs.items() if isinstance(a[1], BaseTempJsonField)])
        return super().__new__(mcs, name, bases, attrs, **kwargs)


class ModelWithJsonFields(models.Model, metaclass=ModelWithJsonBase):

    _meta_json_fields: Dict[str, BaseTempJsonField] = None
    _json_fields: List[BaseTempJsonField] = None

    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        super(ModelWithJsonFields, self).__init__(*args, **kwargs)
        self._json_fields = []
        for field_name, field_json in self._meta_json_fields.items():
            field_json_copy = copy.deepcopy(field_json)
            field_json_copy.attach_to_instance(self)
            self._json_fields.append(field_json_copy)
            setattr(self, field_name, field_json_copy)

    def save(self, *args, **kwargs):
        if not kwargs.pop('ignore_json_fields', None):
            for f in self._json_fields:
                if f.init_value == f.get_persistent_field():
                    f.update_persistent_field()
                else:
                    f.update()
        return super(ModelWithJsonFields, self).save(*args, **kwargs)
