from datetime import timedelta


class JWTConfig:
    secret: str = None
    expiration: timedelta = None

    @classmethod
    def set_secret(cls, secret: str) -> None:
        cls.secret = secret

    @classmethod
    def set_expiration(cls, exp: timedelta) -> None:
        cls.expiration = exp
