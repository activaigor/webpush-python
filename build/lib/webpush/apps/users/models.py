from typing import List

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, Group
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

from webpush.lib.instances import JWTPermissions, JWTChannelPermissions
from webpush.apps.users.constants import PermissionsGroups
from webpush.apps.users.managers import CustomUserManager


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=True)
    is_for_monitoring = models.NullBooleanField(default=None, null=True)
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    _jwt_permissions: JWTPermissions = None

    @property
    def get_short_name(self) -> str:
        return self.email

    def can_send_for_channel(self, channel_id: int) -> bool:
        if channel_id is None:
            return False
        perms = self.get_channel_permissions(channel_id)
        return perms.can_send if perms is not None else False

    def get_channels_for_read(self) -> List[int]:
        if self._jwt_permissions is None:
            return []
        return [c.channel_id for c in self._jwt_permissions.channels]

    def can_retrieve_from_channel(self, channel_id: int) -> bool:
        if channel_id is None:
            return False
        perms = self.get_channel_permissions(channel_id)
        return perms.can_retrieve if perms is not None else False

    def get_channel_permissions(self, channel_id: int) -> JWTChannelPermissions:
        if self._jwt_permissions is not None:
            for p in self._jwt_permissions.channels:
                if p.channel_id == channel_id:
                    return p

    def add_to_managers_group(self) -> None:
        try:
            group = Group.objects.get(name=PermissionsGroups.MANAGERS)
        except Group.DoesNotExist:
            pass
        else:
            self.groups.add(group)

    def set_jwt_permissions(self, permission: JWTPermissions) -> None:
        self._jwt_permissions = permission

    def __str__(self):
        return self.email
