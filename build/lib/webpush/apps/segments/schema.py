import json
from json import JSONEncoder

from schematics import Model
from schematics.types import IntType, ModelType, ListType, StringType

from webpush.apps.segments.constants import SegmentElementConditions, SegmentBlockOperators, SegmentElementAttributes


class SegmentJSONEncoder(JSONEncoder):

    def default(self, o):
        if isinstance(o, Model):
            return dict(o._data)
        return super(SegmentJSONEncoder, self).default(o)


class ChannelSegmentElement(Model):
    attribute = StringType(max_length=100, choices=SegmentElementAttributes.choices, required=True)
    condition = StringType(max_length=3, choices=SegmentElementConditions.choices, required=True)
    value_str = StringType(max_length=255, required=True)


class ChannelSegmentBlock(Model):
    inner_blocks = ListType(ModelType('ChannelSegmentBlock'), required=False, default=[])
    union_operator = StringType(max_length=100, choices=SegmentBlockOperators.choices, required=False, default=None)
    elements = ListType(ModelType(ChannelSegmentElement), required=False, default=[])
    elements_union_operator = StringType(max_length=100, choices=SegmentBlockOperators.choices, required=False,
                                         default=None)


class ChannelSegmentRootBlock(ChannelSegmentBlock):
    pass


class ChannelSegment(Model):
    root_block = ModelType(ChannelSegmentRootBlock)
    channel_ids = ListType(IntType(), required=False, default=[])

    def to_json(self) -> str:
        return json.dumps(dict(self._data), cls=SegmentJSONEncoder)

    def add_element(self, attribute: str, condition: str, value_str: str) -> None:
        if self.root_block is None:
            self.root_block = ChannelSegmentRootBlock({})
        self.root_block.elements.append(ChannelSegmentElement({
            'attribute': attribute,
            'condition': condition,
            'value_str': value_str
        }))

    def set_elements_union_operator(self, operator: str):
        self.root_block.elements_union_operator = operator
