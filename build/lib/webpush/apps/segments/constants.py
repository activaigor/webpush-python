from webpush.apps.segments.values import DateTimeStringFormat, IntStringFormat, PlainStringFormat, KeyValuePair


class SegmentBlockOperators:

    OR = 'or'
    AND = 'and'

    choices = (OR, AND)


class SegmentElementAttributes:

    USER_SUBSCRIBE_DATE = 'user_subscribe_date'
    USER_LAST_ACTIVITY = 'user_last_activity'
    USER_ACTIVITY_COUNTER = 'user_activity_counter'
    USER_COUNTRY = 'user_country'
    USER_BROWSER = 'user_browser'
    USER_BROWSER_LANGUAGE = 'user_browser_language'
    USER_CITY = 'user_city'
    CHANNEL_CUSTOM_ATTR = 'channel_custom_attribute'

    FORMAT_USER_SUBSCRIBE_DATE = DateTimeStringFormat
    FORMAT_USER_LAST_ACTIVITY = DateTimeStringFormat
    FORMAT_USER_ACTIVITY_COUNTER = IntStringFormat
    FORMAT_USER_COUNTRY = PlainStringFormat
    FORMAT_USER_BROWSER = PlainStringFormat
    FORMAT_USER_BROWSER_LANG = PlainStringFormat
    FORMAT_USER_CITY = PlainStringFormat
    FORMAT_CHANNEL_CUSTOM_ATTR = KeyValuePair

    choices = (USER_SUBSCRIBE_DATE, USER_LAST_ACTIVITY, USER_ACTIVITY_COUNTER,
               USER_COUNTRY, USER_BROWSER, USER_CITY, USER_BROWSER_LANGUAGE,
               CHANNEL_CUSTOM_ATTR)


class SegmentElementConditions:
    EQUAL = '=='
    NOT_EQUAL = '!='
    LESS = '<'
    GREATER = '>'
    LESS_EQUAL = '<='
    GREATER_EQUAL = '>='

    choices = (EQUAL, NOT_EQUAL, LESS, GREATER, LESS_EQUAL,
               GREATER_EQUAL)
