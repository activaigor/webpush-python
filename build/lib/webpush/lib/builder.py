import abc
import datetime
from typing import Type, List

import jwt

from webpush.lib.conf import JWTConfig
from webpush.lib.instances import JWTRefInstance, UserJWTInstance, ServiceJWTInstance, JWTTypes, JWTChannelPermissions


class BaseJWTBuilder:
    instance_class: Type[JWTRefInstance] = None

    @classmethod
    def decode(cls, token: str) -> JWTRefInstance:
        """
        Decode token with JWT_SECRET_KEY and return it's payload
        Is several cases may raise exceptions:
        - jwt.ExpiredSignature if JWT token has been expired
        - jwt.InvalidTokenError if provided token is invalid
        - jwt.DecodeError if token format is not valid
        :param token: JWT token
        :return: dict of payload
        """
        assert JWTConfig.secret is not None, 'You need to specify a secret key used by JWT module'
        assert JWTConfig.expiration is not None, 'You need to set an expiration time for jwt tokens'

        payload_dict = jwt.decode(token, JWTConfig.secret, algorithms=['HS256'])
        if cls.instance_class is not None:
            payload_wrapper = cls.instance_class
        else:
            payload_wrapper = cls._find_wrapper_for_payload(payload_dict)
        payload_dict.update({'token': token})
        return payload_wrapper(**payload_dict)

    @classmethod
    def _find_wrapper_for_payload(cls, payload: dict) -> Type[JWTRefInstance]:
        raise NotImplementedError()


class JWTRefBuilder(BaseJWTBuilder):
    instance_class: Type[JWTRefInstance] = JWTRefInstance
    instance: JWTRefInstance = None

    def __init__(self, consumer_id: int):
        assert JWTConfig.secret is not None, 'You need to specify a secret key used by JWT module'
        assert JWTConfig.expiration is not None, 'You need to set an expiration time for jwt tokens'
        self.consumer_id = consumer_id
        self.instance = self.instance_class(consumer_id=self.consumer_id)
        self.token = None

    def build(self) -> None:
        """
        Will build a JWT token, using JWT-secret from project settings
        :return: None
        """
        instance_data = self.instance.to_representation()
        self.token = jwt.encode(instance_data, JWTConfig.secret).decode('utf-8')

    @abc.abstractmethod
    def add_data(self, **kwargs) -> None:
        pass

    @classmethod
    def _find_wrapper_for_payload(cls, payload: dict) -> Type[JWTRefInstance]:
        raise NotImplementedError()


class UserJWTBuilder(JWTRefBuilder):
    instance_class = UserJWTInstance
    instance: UserJWTInstance = None

    def __init__(self, consumer_id: int, email: str = None):
        super(UserJWTBuilder, self).__init__(consumer_id)
        self.email = email
        self.instance.set_expire(datetime.datetime.utcnow() + JWTConfig.expiration)
        self.instance.set_email(self.email)

    def add_data(self, channels: List[int] = None, **kwargs) -> None:
        if channels:
            self.instance.set_channels(channels)

    def add_channel_permission(self, channel_id: int, can_edit: bool = False, can_send: bool = False,
                               can_retrieve: bool = False) -> None:
        channels_permissions = self.instance.permissions.get('channels', [])
        channels_permissions.append(JWTChannelPermissions(
            channel_id=channel_id,
            can_edit=can_edit,
            can_send=can_send,
            can_retrieve=can_retrieve
        ))
        self.instance.set_permissions(channels=channels_permissions)

    @classmethod
    def decode(cls, token) -> UserJWTInstance:
        return super(UserJWTBuilder, cls).decode(token)

    @classmethod
    def _find_wrapper_for_payload(cls, payload: dict) -> Type[JWTRefInstance]:
        raise NotImplementedError()


class ServiceJWTBuilder(JWTRefBuilder):
    instance_class = ServiceJWTInstance
    instance: ServiceJWTInstance = None

    def add_data(self, **kwargs) -> None:
        pass

    @classmethod
    def _find_wrapper_for_payload(cls, payload: dict) -> Type[JWTRefInstance]:
        raise NotImplementedError()


class GenericJWTBuilder(BaseJWTBuilder):

    @classmethod
    def _find_wrapper_for_payload(cls, payload: dict) -> Type[JWTRefInstance]:
        consumer_type = payload.get('consumer_type')
        if consumer_type == JWTTypes.USER:
            return UserJWTInstance
        elif consumer_type == JWTTypes.SERVICE:
            return ServiceJWTInstance
