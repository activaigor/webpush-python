from typing import List

from webpush.lib.statements.decomposer.base import BaseBlockDecomposer, BaseDecomposerFunc, BaseDecomposeResult


class StringDecomposeResult(BaseDecomposeResult):

    value: str = None

    def _and_value(self, value: str) -> str:
        return f'({self.value}) and ({value})'

    def _or_value(self, value: str) -> str:
        return f'({self.value}) or ({value})'


class StringDecomposerFunc(BaseDecomposerFunc):

    @classmethod
    def and_operator(cls, elements: List[str]) -> StringDecomposeResult:
        return StringDecomposeResult(value=' and '.join(elements))

    @classmethod
    def or_operator(cls, elements: List[str]) -> StringDecomposeResult:
        return StringDecomposeResult(value=' or '.join(elements))


class StringBlockDecomposer(BaseBlockDecomposer):

    blocks: List['StringBlockDecomposer'] = None

    class Meta:
        decomposer_func = StringDecomposerFunc
