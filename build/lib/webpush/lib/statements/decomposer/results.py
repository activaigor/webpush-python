from django.db.models import Q

from webpush.lib.statements.decomposer.base import BaseDecomposeResult


class DjangoQueryDecomposeResult(BaseDecomposeResult):

    value: Q = None

    def __init__(self, value: Q):
        super(DjangoQueryDecomposeResult, self).__init__(value=value)

    def _and_value(self, value: Q) -> Q:
        return self.value & value

    def _or_value(self, value: Q) -> Q:
        return self.value | value
