import abc
from abc import ABCMeta
from typing import List, Any, Type

from webpush.lib.statements.decomposer.constants import UnionOperatorNames


class IDecomposeResult(metaclass=abc.ABCMeta):
    value: Any = None


class IDecomposer(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def decompose(self) -> Any:
        """
        After decomposer had being initiated you can call this method to receive a decomposed value of your
        decomposer blocks. The value which will be returned depends on IDecomposeResult that was used during
        a decomposition process.
        :return: IDecomposeResult.value
        """
        pass


class IDecomposerFunc(metaclass=abc.ABCMeta):

    @classmethod
    @abc.abstractmethod
    def and_operator(cls, elements: list) -> IDecomposeResult:
        pass

    @classmethod
    @abc.abstractmethod
    def or_operator(cls, elements: list) -> IDecomposeResult:
        pass

    @classmethod
    @abc.abstractmethod
    def default_operator(cls, elements: list) -> IDecomposeResult:
        pass


class IUnionResultsOperator(metaclass=abc.ABCMeta):
    operator_name: str = None

    @abc.abstractmethod
    def operate(self, result1: IDecomposeResult, result2: IDecomposeResult) -> IDecomposeResult:
        pass


class IUnionElementsOperator(metaclass=abc.ABCMeta):
    operator_name: str = None
    func: Type[IDecomposerFunc] = None

    @abc.abstractmethod
    def operate(self, elements: list) -> IDecomposeResult:
        pass


class IBlockDecomposer(IDecomposer, metaclass=ABCMeta):
    union_operator: IUnionResultsOperator = None
    blocks_union_operator: IUnionResultsOperator = None
    elements_union_operator: IUnionElementsOperator = None
    elements: list = None
    blocks: List['IBlockDecomposer'] = None

    class Meta:
        decomposer_func: IDecomposerFunc = None

    @property
    @abc.abstractmethod
    def elements_block(self) -> 'IBlockDecomposer':
        pass


class BaseDecomposeResult(IDecomposeResult):

    def __init__(self, value: Any):
        self.value = value

    def __and__(self, other: 'BaseDecomposeResult') -> 'BaseDecomposeResult':
        if other is not None:
            return self.init_result_from_value(self._and_value(other.value))
        else:
            return self

    def __or__(self, other: 'BaseDecomposeResult') -> 'BaseDecomposeResult':
        if other is not None:
            return self.init_result_from_value(self._or_value(other.value))
        else:
            return self

    @classmethod
    def init_result_from_value(cls, value) -> 'BaseDecomposeResult':
        return cls(value=value)

    def _and_value(self, value: Any) -> Any:
        raise NotImplementedError()

    def _or_value(self, value: Any) -> Any:
        raise NotImplementedError()


class EmptyDecomposeResult(BaseDecomposeResult):
    
    def __init__(self, value: Any = None):
        super(EmptyDecomposeResult, self).__init__(value)

    def _and_value(self, value: Any) -> Any:
        return None

    def _or_value(self, value: Any) -> Any:
        return None


class UnionResultsOperator(IUnionResultsOperator):

    def __init__(self, operator_name: str):
        self.operator_name = operator_name

    def operate(self, result1: BaseDecomposeResult, result2: BaseDecomposeResult) -> BaseDecomposeResult:
        if self.operator_name == UnionOperatorNames.AND:
            return result1 & result2
        elif self.operator_name == UnionOperatorNames.OR:
            return result1 | result2


class BaseDecomposerFunc(IDecomposerFunc):

    @classmethod
    def default_operator(cls, elements: list) -> BaseDecomposeResult:
        raise NotImplementedError()

    @classmethod
    def and_operator(cls, elements: list) -> BaseDecomposeResult:
        raise NotImplementedError()

    @classmethod
    def or_operator(cls, elements: list) -> BaseDecomposeResult:
        raise NotImplementedError()


class UnionElementsOperator(IUnionElementsOperator):

    func: Type[BaseDecomposerFunc] = None

    def __init__(self, operator_name: str, func: Type[BaseDecomposerFunc]):
        self.operator_name = operator_name
        self.func = func

    def operate(self, elements: list) -> BaseDecomposeResult:
        if self.operator_name == UnionOperatorNames.AND:
            return self.func.and_operator(elements)
        elif self.operator_name == UnionOperatorNames.OR:
            return self.func.or_operator(elements)
        else:
            return self.func.default_operator(elements)


class BaseBlockDecomposer(IBlockDecomposer):

    elements: list = None
    blocks: List['BaseBlockDecomposer'] = None
    elements_union_operator: UnionElementsOperator = None

    class Meta:
        decomposer_func = BaseDecomposerFunc

    def __init__(self, blocks: list = None, blocks_union_operator: str = None,
                 elements: list = None, elements_union_operator: str = None, union_operator: str = None):
        assert (blocks is not None) or (elements is not None), "Either blocks or elements should be defined"
        self.blocks = blocks
        self.blocks_union_operator = UnionResultsOperator(operator_name=blocks_union_operator)
        self.elements = elements
        self.elements_union_operator = UnionElementsOperator(operator_name=elements_union_operator,
                                                             func=self.Meta.decomposer_func)
        self.union_operator = UnionResultsOperator(operator_name=union_operator)

    @property
    def elements_block(self) -> 'BaseBlockDecomposer':
        return self.__make_block_from_elements(elements=self.elements,
                                               elements_union_operator=self.elements_union_operator.operator_name)

    def decompose(self) -> Any:
        """
        :return: Any
        :exception BaseDecomposerException: may be raised when decompose failed
        """
        result = self._decompose()
        return result.value

    def _decompose(self) -> BaseDecomposeResult:
        block_result = None
        elements_result = None

        if self.elements and not self.blocks:
            return self.elements_union_operator.operate(self.elements)
        elif not self.elements and not self.blocks:
            return EmptyDecomposeResult()
        else:
            if self.blocks:
                block_result = self.__aggregate_blocks_results([b._decompose() for b in self.blocks])
            if self.elements:
                elements_result = self.elements_block._decompose()
            if block_result and elements_result:
                result = self.union_operator.operate(block_result, elements_result)
            else:
                result = block_result or elements_result
            return result

    def __aggregate_blocks_results(self, results: List[BaseDecomposeResult]) -> BaseDecomposeResult:
        prev_res = None
        result = None
        for result in results:
            if prev_res is not None:
                result = self.blocks_union_operator.operate(prev_res, result)
            prev_res = result
        return result

    @classmethod
    def __make_block_from_elements(cls, elements: list, elements_union_operator: str) -> 'BaseBlockDecomposer':
        return cls(elements=elements, elements_union_operator=elements_union_operator)
