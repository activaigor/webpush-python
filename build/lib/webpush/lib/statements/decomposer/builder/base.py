import abc
from typing import Any, List

from webpush.lib.statements.decomposer.base import IBlockDecomposer, BaseBlockDecomposer


class IDecomposerBlockBuilder(metaclass=abc.ABCMeta):

    block_source: Any = None

    class Meta:
        decomposer: IBlockDecomposer = None

    @abc.abstractmethod
    def build(self) -> IBlockDecomposer:
        pass


class BaseDecomposerBlockBuilder(IDecomposerBlockBuilder):

    class Meta:
        decomposer = BaseBlockDecomposer

    def __init__(self, block_source: Any):
        self.block_source = block_source

    def build(self) -> BaseBlockDecomposer:
        sub_block_builders = [self.make_builder_from_source(s) for s in self._get_sub_block_sources()]
        if not sub_block_builders:
            return self.Meta.decomposer(elements=self._get_elements(),
                                        elements_union_operator=self._get_elements_union_operator())
        else:
            return self.Meta.decomposer(blocks=[sub_block_builder.build() for sub_block_builder in sub_block_builders],
                                        blocks_union_operator=self._get_blocks_union_operator(),
                                        elements=self._get_elements(),
                                        elements_union_operator=self._get_elements_union_operator(),
                                        union_operator=self._get_union_operator())

    @classmethod
    def make_builder_from_source(cls, block_source: Any) -> 'BaseDecomposerBlockBuilder':
        return cls(block_source=block_source)

    def _get_sub_block_sources(self) -> List[Any]:
        raise NotImplementedError()

    def _get_elements(self) -> List[Any]:
        raise NotImplementedError()

    def _get_union_operator(self) -> str:
        raise NotImplementedError()

    def _get_elements_union_operator(self) -> str:
        raise NotImplementedError()

    def _get_blocks_union_operator(self) -> str:
        raise NotImplementedError()
