from typing import List

from webpush.lib.statements.decomposer.builder.base import BaseDecomposerBlockBuilder
from webpush.lib.statements.decomposer.str import StringBlockDecomposer


class ArrayDecomposerBlockBuilder(BaseDecomposerBlockBuilder):

    block_source: dict = None

    class Meta:
        decomposer = StringBlockDecomposer

    def _get_sub_block_sources(self) -> List[dict]:
        return self.block_source.get('blocks', [])

    def _get_elements(self) -> List[str]:
        return self.block_source.get('elements', [])

    def _get_union_operator(self) -> str:
        return self.block_source.get('union')

    def _get_elements_union_operator(self) -> str:
        return self.block_source.get('elements_union')

    def _get_blocks_union_operator(self) -> str:
        return self.block_source.get('block_union')
