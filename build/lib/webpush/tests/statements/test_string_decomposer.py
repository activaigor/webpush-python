from unittest import TestCase

from webpush.lib.statements.decomposer.constants import UnionOperatorNames
from webpush.lib.statements.decomposer.str import StringBlockDecomposer


class StringDecomposerTest(TestCase):

    def setUp(self) -> None:
        pass

    def test_string_decomposer_works_properly(self):
        block_1 = StringBlockDecomposer(elements=['a > 1', 'a < 3'], elements_union_operator=UnionOperatorNames.AND)
        block_2 = StringBlockDecomposer(elements=['c > 5', 'c < 8'], elements_union_operator=UnionOperatorNames.AND)
        root_block = StringBlockDecomposer(blocks=[block_1, block_2], elements=['b == 1', 'b == 3'],
                                           blocks_union_operator=UnionOperatorNames.OR,
                                           elements_union_operator=UnionOperatorNames.OR,
                                           union_operator=UnionOperatorNames.AND)
        decomposed_value = root_block.decompose()
        self.assertEqual('((a > 1 and a < 3) or (c > 5 and c < 8)) and (b == 1 or b == 3)', decomposed_value)
