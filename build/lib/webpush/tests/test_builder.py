import datetime

import jwt
from unittest import TestCase
from freezegun import freeze_time

from webpush.lib.conf import JWTConfig


class TestJWTBuilder(TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        JWTConfig.set_secret('super_secret')
        JWTConfig.set_expiration(datetime.timedelta(days=1))
        super(TestJWTBuilder, cls).setUpClass()

    def test_user_jwt_build_token_encode(self):
        from webpush.lib.builder import UserJWTBuilder
        consumer_id = 1
        builder = UserJWTBuilder(consumer_id)
        builder.build()
        self.assertIsInstance(builder.token, str)

    def test_user_jwt_build_token_with_permissions(self):
        from webpush.lib.builder import UserJWTBuilder
        consumer_id = 1
        builder = UserJWTBuilder(consumer_id)
        builder.add_channel_permission(channel_id=1, can_edit=True)
        builder.build()
        self.assertIsInstance(builder.token, str)

    def test_user_jwt_build_token_and_decode_with_permissions(self):
        from webpush.lib.builder import UserJWTBuilder
        consumer_id = 1
        builder = UserJWTBuilder(consumer_id)
        builder.add_channel_permission(channel_id=1, can_edit=True)
        builder.build()
        decoded = UserJWTBuilder.decode(builder.token)
        self.assertIsNotNone(decoded)
        self.assertIsNotNone(decoded.get('permissions'))
        self.assertIsInstance(decoded['permissions'], dict)
        self.assertIsNotNone(decoded['permissions'].get('channels'))
        self.assertIsInstance(decoded['permissions'].get('channels'), list)
        self.assertEqual(len(decoded['permissions']['channels']), 1)
        channel_permission = decoded['permissions']['channels'][0]
        self.assertEqual(channel_permission['channel_id'], 1)
        self.assertTrue(channel_permission['can_edit'])

    def test_user_jwt_build_token_and_decode(self):
        from webpush.lib.builder import UserJWTBuilder
        consumer_id = 1
        builder = UserJWTBuilder(consumer_id)
        builder.build()
        token = builder.token
        payload = UserJWTBuilder.decode(token)
        self.assertIsInstance(payload, dict)
        self.assertEqual(payload.get('consumer_id'), consumer_id)

    def test_user_jwt_decode_invalid_token(self):
        from webpush.lib.builder import UserJWTBuilder
        with self.assertRaises(jwt.DecodeError) as exc:
            UserJWTBuilder.decode('asdjashd98as')
        self.assertIsInstance(exc.exception, jwt.DecodeError)

    def test_user_jwt_decode_token_expired(self):
        from webpush.lib.builder import UserJWTBuilder

        def expired_date():
            return datetime.datetime.utcnow() - (JWTConfig.expiration + datetime.timedelta(minutes=1))

        consumer_id = 1
        with freeze_time(expired_date):
            builder = UserJWTBuilder(consumer_id)
            builder.build()
        with self.assertRaises(jwt.ExpiredSignatureError) as exc:
            token = builder.token
            UserJWTBuilder.decode(token)
        self.assertIsInstance(exc.exception, jwt.ExpiredSignatureError)
